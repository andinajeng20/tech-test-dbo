<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>i_Collaborations_air ai-search</name>
   <tag></tag>
   <elementGuidId>22477d46-ab83-48bc-9026-32398d024a5b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.nav-link > i.air.ai-search</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='__next']/nav/div/div/div[2]/ul/li/button/i</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>5a178e5d-1fca-4fa4-8ab8-5bc3bb00ff44</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>air ai-search</value>
      <webElementGuid>01ec08f7-c37d-4ee7-9070-1f9d0b97b7ad</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;__next&quot;)/nav[@class=&quot;navbar navbar-light navbar-expand-lg fixed-top
          transparent&quot;]/div[@class=&quot;navbar-main&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;desktop-menu menu-right d-none d-lg-flex&quot;]/ul[@class=&quot;navbar-nav ml-auto&quot;]/li[@class=&quot;nav-item&quot;]/button[@class=&quot;nav-link&quot;]/i[@class=&quot;air ai-search&quot;]</value>
      <webElementGuid>c03100fc-2773-4b0d-ad42-9d93263c653c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='__next']/nav/div/div/div[2]/ul/li/button/i</value>
      <webElementGuid>11dadfbd-3822-4620-a101-34f56d612b40</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/ul/li/button/i</value>
      <webElementGuid>31440879-4755-470b-8278-53c72ab7b598</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
