<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_See All</name>
   <tag></tag>
   <elementGuidId>df581453-e36c-4f5d-bd41-787b3b0c4056</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.btn.btn-link.d-flex.align-items-center > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='__next']/nav/div[8]/div/div/div/ul/li/a/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>fd933a76-5d7b-4f0c-8507-955932c34bcd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>See All</value>
      <webElementGuid>3342d2fb-dc6d-4ec9-b72e-b05e805bd263</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;__next&quot;)/nav[@class=&quot;navbar navbar-light navbar-expand-lg fixed-top
          transparent
          is-scrolled&quot;]/div[@class=&quot;megamenu&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;menu-wrapper type-1&quot;]/div[@class=&quot;col-first py-5&quot;]/ul[@class=&quot;menu-categories&quot;]/li[@class=&quot;categories-item active scroll-fadeInUp fadeInUp d0&quot;]/a[@class=&quot;btn btn-link    d-flex align-items-center&quot;]/span[1]</value>
      <webElementGuid>12fd0588-0c9e-48c0-961c-7a0650440104</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='__next']/nav/div[8]/div/div/div/ul/li/a/span</value>
      <webElementGuid>9a7dfceb-d97d-4c65-b474-e6ad8543e68f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Proceed to Checkout'])[1]/following::span[1]</value>
      <webElementGuid>128623cb-5d1b-4788-9fd2-c72ea1dc7a09</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Gazelle'])[2]/preceding::span[1]</value>
      <webElementGuid>26b8a44c-90e6-4d19-b00a-f6dfb963bd00</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Retrograde'])[2]/preceding::span[1]</value>
      <webElementGuid>bc7c098a-5fe3-4ee4-a738-0190521ce357</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='See All']/parent::*</value>
      <webElementGuid>9d882cb1-cac8-4899-ba98-9c1fa633f3db</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/a/span</value>
      <webElementGuid>fba0e88a-d7ac-441a-8c3c-ac724e68a2ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'See All' or . = 'See All')]</value>
      <webElementGuid>dc52538b-f628-4b0c-bf61-45c8b7a0b46f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
