<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h4_Retrograde Slip On Kawung Cream</name>
   <tag></tag>
   <elementGuidId>060295b0-4a7f-40d7-b9d0-84647d305406</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='__next']/main/section/div/div/div[2]/div/div/div/div/div/a/div/div[2]/div/h4</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h4</value>
      <webElementGuid>9ac82715-3f90-458d-9c43-b91bad2e1012</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card-title product-title</value>
      <webElementGuid>4db0232f-285a-4a4d-977b-02c183fb13cb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Retrograde Slip On Kawung Cream</value>
      <webElementGuid>cea62b7e-6b3e-417d-b49e-dae615ceb917</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;__next&quot;)/main[@class=&quot;animated fadeIn&quot;]/section[@class=&quot;pb-main sc-shop-all&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row row-4&quot;]/div[@class=&quot;col-lg-10&quot;]/div[@class=&quot;infinite-scroll-component__outerdiv&quot;]/div[@class=&quot;infinite-scroll-component shop-infinite&quot;]/div[@class=&quot;row row-4&quot;]/div[@class=&quot;col-6 col-lg-4&quot;]/div[1]/a[@class=&quot;card card-boxless product-card product-card scroll-fadeInUp fadeInUp d-8&quot;]/div[@class=&quot;row row-0&quot;]/div[@class=&quot;col-body col-12&quot;]/div[@class=&quot;card-body px-sm-down-0 pt-2&quot;]/h4[@class=&quot;card-title product-title&quot;]</value>
      <webElementGuid>a0f33f5e-d581-44f1-b653-4a3b859f2219</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='__next']/main/section/div/div/div[2]/div/div/div/div/div/a/div/div[2]/div/h4</value>
      <webElementGuid>1cbefa8a-c422-4fde-b57e-2387cd749d6c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All Size'])[1]/following::h4[1]</value>
      <webElementGuid>6602dbac-c9a4-44de-93ee-fbfef81e71d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Size'])[1]/following::h4[1]</value>
      <webElementGuid>db7b7a78-b1b9-4a77-bebd-dd08e24c16c5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Retrograde Slip On Kawung Camel'])[1]/preceding::h4[1]</value>
      <webElementGuid>c86c083d-ad1e-4feb-ae72-6d9e49f128d3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Retrograde Slip On Kawung Black'])[1]/preceding::h4[2]</value>
      <webElementGuid>bd852146-a2c6-4a0c-afe2-6588c9c17d19</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Retrograde Slip On Kawung Cream']/parent::*</value>
      <webElementGuid>25c32ace-4672-4e16-a6a8-d46234747bc1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/div/a/div/div[2]/div/h4</value>
      <webElementGuid>95e9b1df-a691-436a-9ab0-1007fb29a427</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h4[(text() = 'Retrograde Slip On Kawung Cream' or . = 'Retrograde Slip On Kawung Cream')]</value>
      <webElementGuid>51be6038-c454-4a00-8578-eaade4ca1a6b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
