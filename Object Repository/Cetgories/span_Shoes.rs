<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Shoes</name>
   <tag></tag>
   <elementGuidId>1115da1d-47ba-4e6d-9729-537fe30226fd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='__next']/main/section/div/div/div/div/div[2]/ul/li/div/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>ef73d242-c41e-408f-aae9-f2d9d47d374a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>filter-label</value>
      <webElementGuid>c7d1b9ab-7b19-43e8-860a-56a2903b283a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Shoes</value>
      <webElementGuid>37adc63c-1409-48f7-b2ba-022602ea678e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;__next&quot;)/main[@class=&quot;animated fadeIn&quot;]/section[@class=&quot;pb-main sc-shop-all&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row row-4&quot;]/div[@class=&quot;col-lg-2 d-none d-lg-block col-filter&quot;]/div[@class=&quot;filter-wrapper&quot;]/div[@class=&quot;filter-category&quot;]/ul[@class=&quot;filter-list&quot;]/li[@class=&quot;filter-item&quot;]/div[@class=&quot;w-100&quot;]/span[@class=&quot;filter-label&quot;]</value>
      <webElementGuid>f6d5dac3-2902-4ea6-850e-38b36bb47d33</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='__next']/main/section/div/div/div/div/div[2]/ul/li/div/span</value>
      <webElementGuid>b20af83e-3ed2-4589-9a71-9f805db2fb8b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Categories'])[1]/following::span[1]</value>
      <webElementGuid>d531bbbb-e398-4554-aa47-a83eee0044ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Price: High to Low'])[1]/following::span[1]</value>
      <webElementGuid>945f06e2-7cbe-47a5-a690-a6f255ca896b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Collection'])[1]/preceding::span[1]</value>
      <webElementGuid>1e25a20a-0cd0-4306-bdbf-7f78f013fd0f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Compass® Slip On Kawung'])[1]/preceding::span[1]</value>
      <webElementGuid>ff198a7f-a27e-4364-bfcb-14cec0e2eed7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/ul/li/div/span</value>
      <webElementGuid>1326a8fe-31ee-422f-bd1a-dedd8323febe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Shoes' or . = 'Shoes')]</value>
      <webElementGuid>4c1d4d4a-7fcc-4b37-b90b-8d870bb997ae</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
