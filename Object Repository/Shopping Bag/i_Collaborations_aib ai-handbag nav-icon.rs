<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>i_Collaborations_aib ai-handbag nav-icon</name>
   <tag></tag>
   <elementGuidId>831b22f2-4aee-4f74-b818-0f5f3592467c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.nav-link > div.position-relative > i.aib.ai-handbag.nav-icon</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='__next']/nav/div/div/div[2]/ul/li[3]/button/div/i</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>9d2b8026-030f-48fb-ac1b-40525d30e498</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>aib ai-handbag nav-icon</value>
      <webElementGuid>00d39c0f-ff25-44a1-8dd3-5c10dba19bcb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;__next&quot;)/nav[@class=&quot;navbar navbar-light navbar-expand-lg fixed-top
          
          is-scrolled&quot;]/div[@class=&quot;navbar-main&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;desktop-menu menu-right d-none d-lg-flex&quot;]/ul[@class=&quot;navbar-nav ml-auto&quot;]/li[@class=&quot;nav-item&quot;]/button[@class=&quot;nav-link&quot;]/div[@class=&quot;position-relative&quot;]/i[@class=&quot;aib ai-handbag nav-icon&quot;]</value>
      <webElementGuid>83d99b71-422c-4707-b2ac-88099947dc07</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='__next']/nav/div/div/div[2]/ul/li[3]/button/div/i</value>
      <webElementGuid>d0ac1907-a884-43cb-af8e-d354049406a4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[3]/button/div/i</value>
      <webElementGuid>b2253f72-a153-4682-b21e-442a40af0ecb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
