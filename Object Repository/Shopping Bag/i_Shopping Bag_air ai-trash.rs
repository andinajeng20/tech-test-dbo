<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>i_Shopping Bag_air ai-trash</name>
   <tag></tag>
   <elementGuidId>41cd0739-4f02-4bd4-b951-160e439ddab5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>i.air.ai-trash</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='__next']/nav/div[6]/div/div[3]/div/div/div[2]/div[2]/i</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>d6cc8906-20fa-4c79-9ebf-b0ebd7da75df</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>air ai-trash</value>
      <webElementGuid>b872974f-df4f-4669-825b-ddb548aeeb0d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;__next&quot;)/nav[@class=&quot;navbar navbar-light navbar-expand-lg fixed-top
          
          is-scrolled
          
          shopping-bag-menu-open&quot;]/div[@class=&quot;bag-menu mobile-menu slide-left hide show&quot;]/div[@class=&quot;bag-menu-content&quot;]/div[@class=&quot;bag-menu-content-top&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;product-item&quot;]/div[@class=&quot;col-content&quot;]/div[@class=&quot;btn btn-remove&quot;]/i[@class=&quot;air ai-trash&quot;]</value>
      <webElementGuid>49cd4346-e81c-49ba-9abb-c19f527a37ec</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='__next']/nav/div[6]/div/div[3]/div/div/div[2]/div[2]/i</value>
      <webElementGuid>617b201a-8f1d-48a0-b63e-00635f4e15d7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/i</value>
      <webElementGuid>0829c8a6-5c7a-4846-8325-8342da00d226</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
