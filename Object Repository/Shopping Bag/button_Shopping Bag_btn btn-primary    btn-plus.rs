<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Shopping Bag_btn btn-primary    btn-plus</name>
   <tag></tag>
   <elementGuidId>794bc505-49be-4e4b-a3e5-b2f89de2bf8c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-primary.btn-plus</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='__next']/nav/div[6]/div/div[3]/div/div/div[2]/div/div/button[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>252761ae-2a5f-4571-b358-643783cd377a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary    btn-plus</value>
      <webElementGuid>c04f763f-23ff-4f00-8f1a-a74df05606f4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;__next&quot;)/nav[@class=&quot;navbar navbar-light navbar-expand-lg fixed-top
          
          is-scrolled
          
          shopping-bag-menu-open&quot;]/div[@class=&quot;bag-menu mobile-menu slide-left hide show&quot;]/div[@class=&quot;bag-menu-content&quot;]/div[@class=&quot;bag-menu-content-top&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;product-item&quot;]/div[@class=&quot;col-content&quot;]/div[@class=&quot;att&quot;]/div[@class=&quot;att-item qty-adjust&quot;]/button[@class=&quot;btn btn-primary    btn-plus&quot;]</value>
      <webElementGuid>a4975380-2a13-4a5c-8f52-737cf5c22869</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='__next']/nav/div[6]/div/div[3]/div/div/div[2]/div/div/button[2]</value>
      <webElementGuid>f1c51521-aaa2-4192-8b3a-c08a96140a9a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Log Out'])[1]/following::button[2]</value>
      <webElementGuid>bf287be4-e4ae-4df1-b4bc-21379bec9d19</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Have promo code? Use in check out.'])[1]/preceding::button[1]</value>
      <webElementGuid>2243ec8d-b87c-4673-966b-69a054a5e8f9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Proceed to Checkout'])[1]/preceding::button[1]</value>
      <webElementGuid>1d8da833-9b64-44a3-8f35-cff61a0ca8e9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button[2]</value>
      <webElementGuid>d1c83166-3acc-479e-9dba-715973480a3a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
