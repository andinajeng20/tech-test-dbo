<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Shopping Bag_btn btn-primary    btn-minus</name>
   <tag></tag>
   <elementGuidId>dcc80929-5d84-475c-8b13-cac50771c97b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-primary.btn-minus</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='__next']/nav/div[6]/div/div[3]/div/div/div[2]/div/div/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>25074cf1-6d74-42b9-8e10-3c581a5d2d3c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary    btn-minus</value>
      <webElementGuid>71e4e0ab-4682-4372-8ffa-91833e50053f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;__next&quot;)/nav[@class=&quot;navbar navbar-light navbar-expand-lg fixed-top
          
          is-scrolled
          
          shopping-bag-menu-open&quot;]/div[@class=&quot;bag-menu mobile-menu slide-left hide show&quot;]/div[@class=&quot;bag-menu-content&quot;]/div[@class=&quot;bag-menu-content-top&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;product-item&quot;]/div[@class=&quot;col-content&quot;]/div[@class=&quot;att&quot;]/div[@class=&quot;att-item qty-adjust&quot;]/button[@class=&quot;btn btn-primary    btn-minus&quot;]</value>
      <webElementGuid>bb5d6e03-3e9e-488d-93be-1a171ff7db32</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='__next']/nav/div[6]/div/div[3]/div/div/div[2]/div/div/button</value>
      <webElementGuid>85477b9a-bdd6-437b-b9a3-987a84513efa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Log Out'])[1]/following::button[1]</value>
      <webElementGuid>7df93dde-0617-44d6-9947-693315a2f9b4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Have promo code? Use in check out.'])[1]/preceding::button[2]</value>
      <webElementGuid>41c648ad-ea8e-4c4c-92ff-b1e6e2ba568e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Proceed to Checkout'])[1]/preceding::button[2]</value>
      <webElementGuid>3cad36bf-5e5f-4a94-9cc1-3fd7713be6ba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/button</value>
      <webElementGuid>1955286b-edd1-42d3-b6c7-123672ed6865</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
