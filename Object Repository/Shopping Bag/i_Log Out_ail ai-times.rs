<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>i_Log Out_ail ai-times</name>
   <tag></tag>
   <elementGuidId>633a1620-883f-4328-aa07-8555193c6dd9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.btn.close-button > i.ail.ai-times</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='__next']/nav/div[6]/div/div/i</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>d4cb1ce8-5254-490d-b534-84d3c4290e48</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ail ai-times</value>
      <webElementGuid>167491b1-fd3b-4902-af1b-31d22db2bac4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;__next&quot;)/nav[@class=&quot;navbar navbar-light navbar-expand-lg fixed-top
          
          is-scrolled
          
          shopping-bag-menu-open&quot;]/div[@class=&quot;bag-menu mobile-menu slide-left hide show&quot;]/div[@class=&quot;bag-menu-content&quot;]/div[@class=&quot;btn close-button&quot;]/i[@class=&quot;ail ai-times&quot;]</value>
      <webElementGuid>04aad0cf-716e-4756-9def-d62600cb3c27</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='__next']/nav/div[6]/div/div/i</value>
      <webElementGuid>c34c20c2-7fbe-4378-96c5-b25997dd2345</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/div/div/i</value>
      <webElementGuid>6f63969f-30bc-429b-903e-caad225026ba</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
