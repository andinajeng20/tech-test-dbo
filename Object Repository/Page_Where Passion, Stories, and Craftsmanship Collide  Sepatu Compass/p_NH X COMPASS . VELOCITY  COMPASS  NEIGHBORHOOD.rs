<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_NH X COMPASS . VELOCITY  COMPASS  NEIGHBORHOOD</name>
   <tag></tag>
   <elementGuidId>a24dee6f-87e2-4f8a-af7f-5f16c66e572c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>p.scroll-fadeInUp.fadeInUp.d8</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='__next']/main/section/div/div[2]/div/div/div[2]/div/div/div/p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>920fa27f-8e70-4432-8ed9-53d941a328a7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>scroll-fadeInUp fadeInUp d8</value>
      <webElementGuid>888ca0a1-ab74-4869-979f-f7b0ac83bf3d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>NH X COMPASS . VELOCITY  COMPASS® / NEIGHBORHOOD®</value>
      <webElementGuid>96e0d2d3-be4a-4604-8cbe-3b32666bdb83</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;__next&quot;)/main[@class=&quot;animated fadeIn&quot;]/section[@class=&quot;sc-common-cover&quot;]/div[@class=&quot;cover cover-basic cover-dark sc-home-cover&quot;]/div[@class=&quot;cover-body&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;cover-content mw-md-500px&quot;]/div[@class=&quot;footer-cover d-md-block d-none&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;footer-wrapper&quot;]/div[@class=&quot;col-right ab-testing vwo_loaded vwo_loaded_2&quot;]/p[@class=&quot;scroll-fadeInUp fadeInUp d8&quot;]</value>
      <webElementGuid>9db91a5e-4be7-450e-88d2-9fb2f56bf6fe</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='__next']/main/section/div/div[2]/div/div/div[2]/div/div/div/p</value>
      <webElementGuid>c6b9773d-1fc9-454c-bf1b-8671c75091c6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Shop Now'])[1]/following::p[1]</value>
      <webElementGuid>94cbd6fc-8b95-4256-aec5-12504bf899fa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Gazelle Low Black White'])[1]/following::p[2]</value>
      <webElementGuid>15e41c28-0413-43c6-ab86-8a45bee9095c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Shop Now'])[2]/preceding::p[1]</value>
      <webElementGuid>7e5b87c0-d7c2-4922-9d2e-7c0477aa477d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Scroll for More'])[1]/preceding::p[1]</value>
      <webElementGuid>e51468be-ddba-46e5-ad6a-3b17558e8f6c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='NH X COMPASS . VELOCITY']/parent::*</value>
      <webElementGuid>cb280f45-d80d-426d-a977-1912e2538314</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[2]/div/div/div/p</value>
      <webElementGuid>24582432-2a4e-4156-acbb-0435a4621f9a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'NH X COMPASS . VELOCITY  COMPASS® / NEIGHBORHOOD®' or . = 'NH X COMPASS . VELOCITY  COMPASS® / NEIGHBORHOOD®')]</value>
      <webElementGuid>5c70b06f-1264-4e89-93ed-a4f933fa2f5e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
