<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Close Pop Up</name>
   <tag></tag>
   <elementGuidId>4ffa1756-fdb6-4e21-959f-a4b836929b80</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.close > i.ail.ai-times</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[5]/div/div/div/div/i</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>06297452-c71b-4af7-96a3-27a71e3bfc95</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ail ai-times</value>
      <webElementGuid>ef93a20a-1c53-4126-9633-c33602494aa4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;ReactModal__Body--open&quot;]/div[@class=&quot;ReactModalPortal&quot;]/div[@class=&quot;ReactModal__Overlay ReactModal__Overlay--after-open&quot;]/div[@class=&quot;ReactModal__Content ReactModal__Content--after-open modal modal-sm visitors-modal&quot;]/div[@class=&quot;modal-wrapper&quot;]/div[@class=&quot;close&quot;]/i[@class=&quot;ail ai-times&quot;]</value>
      <webElementGuid>71cbea8f-f3dc-4111-9f6f-3c98e81a3cee</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div/div/div/i</value>
      <webElementGuid>d59e4f26-476d-4669-9eb5-f6c3b892308e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
