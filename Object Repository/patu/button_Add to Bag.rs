<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Add to Bag</name>
   <tag></tag>
   <elementGuidId>70c00fa3-c884-4ef6-91f4-09161d3f4ae6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.content-sizes > button.btn.btn-primary.btn-block</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='__next']/main/section/div/div/div[3]/div/div/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>6bf4fcf4-0c02-445c-a1b7-2333c70cd1a4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary    btn-block</value>
      <webElementGuid>1672d48f-4f33-418d-8441-ee34166afde5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Add to Bag</value>
      <webElementGuid>9a772fd2-bbc2-4262-ad1d-4876f41f26f3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;__next&quot;)/main[@class=&quot;animated fadeIn&quot;]/section[@class=&quot;sc-shop-detail_desktop d-lg-block d-none pb-main&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;grid-wrapper&quot;]/div[@class=&quot;col-right&quot;]/div[@class=&quot;content-sticky&quot;]/div[@class=&quot;content-sizes&quot;]/button[@class=&quot;btn btn-primary    btn-block&quot;]</value>
      <webElementGuid>2f48d4c0-23be-441b-98f2-20dd1d7aa91a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='__next']/main/section/div/div/div[3]/div/div/button</value>
      <webElementGuid>1e9f97a4-1523-4779-a033-afdfeaf6201e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='REMIND ME'])[1]/following::button[1]</value>
      <webElementGuid>19ee9a8b-63a7-4d3b-9ecc-0b95f8dfb130</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[3]/div/div/button</value>
      <webElementGuid>285f743a-6749-4ff3-8582-6b7638f06d71</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Add to Bag' or . = 'Add to Bag')]</value>
      <webElementGuid>723c446a-3cab-4b8c-8cbe-a366d6b814a0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
