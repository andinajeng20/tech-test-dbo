<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Back to Size</name>
   <tag></tag>
   <elementGuidId>5d714696-8b56-4892-b06e-96de9c8f22b0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='__next']/main/section/div/div/div[3]/div/div/div/button/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>a330b015-f491-4d4a-8299-2d6e0af63963</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Back to Size</value>
      <webElementGuid>3aed7130-c6cf-4cfb-8f5a-603c09936c97</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;__next&quot;)/main[@class=&quot;animated fadeIn&quot;]/section[@class=&quot;sc-shop-detail_desktop d-lg-block d-none pb-main&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;grid-wrapper&quot;]/div[@class=&quot;col-right&quot;]/div[@class=&quot;content-sticky&quot;]/div[1]/div[@class=&quot;sizes-header mb-2&quot;]/button[@class=&quot;btn btn-link&quot;]/span[1]</value>
      <webElementGuid>849b4fd0-9b28-4f2c-9348-547588d8f172</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='__next']/main/section/div/div/div[3]/div/div/div/button/span</value>
      <webElementGuid>5989f4f3-aaa7-4a3a-964b-2d7db6e2f745</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='IDR 60'])[1]/following::span[1]</value>
      <webElementGuid>754573ca-2bbb-4101-b5a2-cdb38459033c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Retrograde Slip On Kawung Cream'])[3]/following::span[1]</value>
      <webElementGuid>6c7b4965-6b92-434b-8cde-71eb9756b98b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='US 4 / UK 3 / EUR 34 / CM 22.5'])[1]/preceding::span[1]</value>
      <webElementGuid>1797b77a-c51a-464f-8315-c9cf2ef902d4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='US 4.5 / UK 3.5 / EUR 35 / CM 23.5'])[1]/preceding::span[1]</value>
      <webElementGuid>37df5cfa-8783-4e7b-bef8-77e569a45c49</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Back to Size']/parent::*</value>
      <webElementGuid>a0fcbd64-e2cf-4f92-93f8-7ee1f1c235a5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div/button/span</value>
      <webElementGuid>2d53950e-eb64-4482-b18f-03379de45f00</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Back to Size' or . = 'Back to Size')]</value>
      <webElementGuid>c3e694e5-7b91-40c5-9d4e-2ad1a0f7a04b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
