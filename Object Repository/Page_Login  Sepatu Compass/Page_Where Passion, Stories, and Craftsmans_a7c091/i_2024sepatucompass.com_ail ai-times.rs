<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>i_2024sepatucompass.com_ail ai-times</name>
   <tag></tag>
   <elementGuidId>58bbe801-877a-4862-a255-6da16e8b8bad</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.close > i.ail.ai-times</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[5]/div/div/div/div/i</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
      <webElementGuid>bd367df4-e00d-4a26-acc2-24bd6fcf8997</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ail ai-times</value>
      <webElementGuid>cc7159ca-7507-475a-b20e-9953c60d2f2b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;ReactModal__Body--open&quot;]/div[@class=&quot;ReactModalPortal&quot;]/div[@class=&quot;ReactModal__Overlay ReactModal__Overlay--after-open&quot;]/div[@class=&quot;ReactModal__Content ReactModal__Content--after-open modal modal-sm visitors-modal&quot;]/div[@class=&quot;modal-wrapper&quot;]/div[@class=&quot;close&quot;]/i[@class=&quot;ail ai-times&quot;]</value>
      <webElementGuid>375c87ea-cc5a-4c79-b151-db38839c5731</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div/div/div/i</value>
      <webElementGuid>6b097be3-e4f1-445c-8fb2-f4dd0359f521</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
