<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Register</name>
   <tag></tag>
   <elementGuidId>84bd941e-f13a-406c-94aa-e9eb5ccbc1a3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='__next']/main/div/div[2]/div/div/div/div/div[2]/button/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>6fd2cdc0-15dc-4672-94dc-2ed8b3165092</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Register</value>
      <webElementGuid>82c45e82-094d-44df-b860-4afe0b8dcb12</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;__next&quot;)/main[@class=&quot;animated fadeIn&quot;]/div[@class=&quot;cover cover-basic cover-light sc-login-wrapper d-lg-block d-none&quot;]/div[@class=&quot;cover-body&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;cover-content mw-md-500px&quot;]/div[@class=&quot;form-wrapper&quot;]/div[@class=&quot;form-nav row row-0&quot;]/div[@class=&quot;col-6&quot;]/button[@class=&quot;btn btn-login    btn-block&quot;]/span[1]</value>
      <webElementGuid>a5c29cb6-dd50-4201-9cc9-26c02d8badde</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='__next']/main/div/div[2]/div/div/div/div/div[2]/button/span</value>
      <webElementGuid>f47406ab-f539-4c1e-b852-17444bf78650</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Login'])[2]/following::span[1]</value>
      <webElementGuid>d2a66db4-d18d-4c38-b86a-cc0932e98a5d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Gazelle Low Black White'])[1]/following::span[2]</value>
      <webElementGuid>ece79175-c56b-4d53-85e0-ee60e6eb37cf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='E-mail'])[1]/preceding::span[1]</value>
      <webElementGuid>0ddb56a3-ecc4-4b3e-a2e9-3d6e19fd339c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='E-mail'])[2]/preceding::span[1]</value>
      <webElementGuid>98c98352-9782-4219-b25f-995acac8e388</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/button/span</value>
      <webElementGuid>6c2860b8-03f7-4fa5-ac07-e5fa33e890e9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Register' or . = 'Register')]</value>
      <webElementGuid>f17e367b-b9b9-431e-9504-398aa97008b8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
