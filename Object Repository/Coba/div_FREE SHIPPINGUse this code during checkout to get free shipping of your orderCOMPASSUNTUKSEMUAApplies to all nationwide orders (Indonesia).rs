<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_FREE SHIPPINGUse this code during checkout to get free shipping of your orderCOMPASSUNTUKSEMUAApplies to all nationwide orders (Indonesia)</name>
   <tag></tag>
   <elementGuidId>e7d432bb-3783-4c98-a7f2-ab1a77562b48</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.ReactModal__Overlay.ReactModal__Overlay--after-open</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='`'])[2]/following::div[4]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>71277c15-29af-4bbf-b9c1-f848a46de0a6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ReactModal__Overlay ReactModal__Overlay--after-open</value>
      <webElementGuid>9d7bdeae-e343-40d3-9426-6f2e67e7b712</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>FREE SHIPPINGUse this code during checkout to get free shipping of your order!COMPASSUNTUKSEMUAApplies to all nationwide orders (Indonesia).</value>
      <webElementGuid>689ddb66-d234-49fa-9e80-a22a49722265</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;ReactModal__Body--open&quot;]/div[@class=&quot;ReactModalPortal&quot;]/div[@class=&quot;ReactModal__Overlay ReactModal__Overlay--after-open&quot;]</value>
      <webElementGuid>22323e60-7619-4c02-8ede-ad91b4fe8b29</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='`'])[2]/following::div[4]</value>
      <webElementGuid>a269fe11-60b1-416f-9ee8-815b8db0c17c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alt'])[2]/following::div[4]</value>
      <webElementGuid>9028982d-80f0-4dee-a80b-8864f1f47ad1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div</value>
      <webElementGuid>147c972c-dc41-41b6-a4ca-133db30a0db8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'FREE SHIPPINGUse this code during checkout to get free shipping of your order!COMPASSUNTUKSEMUAApplies to all nationwide orders (Indonesia).' or . = 'FREE SHIPPINGUse this code during checkout to get free shipping of your order!COMPASSUNTUKSEMUAApplies to all nationwide orders (Indonesia).')]</value>
      <webElementGuid>a72e600c-fca1-45a9-b87e-59c543132590</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
