import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('UserStory2/Navigate to Shop'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('pilih patu/h4_Retrograde Slip On Kawung Cream'))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.click(findTestObject('patu/span_Size Chart'))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.click(findTestObject('patu/span_Back to Size'))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.click(findTestObject('patu/span_EU 38'))

WebUI.delay(5)

WebUI.click(findTestObject('patu/button_Add to Bag'))

WebUI.delay(5)

WebUI.takeScreenshot()

