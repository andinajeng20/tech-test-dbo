import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('UserStory1/Open URL'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Logon/i_Collaborations_air ai-search'))

'Search Equal Criteria'
WebUI.setText(findTestObject('Logon/input_Collaborations_search'), 'RETROGRADE LOW CREME GRAPE')

WebUI.sendKeys(findTestObject('Logon/input_Collaborations_search'), Keys.chord(Keys.ENTER))

WebUI.delay(5, FailureHandling.STOP_ON_FAILURE)

WebUI.takeScreenshot()

WebUI.callTestCase(findTestCase('UserStory2/Navigate to Search'), [:], FailureHandling.STOP_ON_FAILURE)

'Search Contain Criteria'
WebUI.setText(findTestObject('Logon/input_Collaborations_search'), 'Hi')

WebUI.sendKeys(findTestObject('Logon/input_Collaborations_search'), Keys.chord(Keys.ENTER))

WebUI.delay(5)

WebUI.takeScreenshot()

WebUI.callTestCase(findTestCase('UserStory2/Navigate to Shop'), [:], FailureHandling.STOP_ON_FAILURE)

'Based on Sort By'
WebUI.click(findTestObject('SortBy/span_Latest Arrival'))

WebUI.click(findTestObject('SortBy/span_Best Seller'))

WebUI.click(findTestObject('SortBy/span_Price Low to High'))

WebUI.click(findTestObject('SortBy/span_Price High to Low'))

'Based on Category'
WebUI.click(findTestObject('Cetgories/span_Shoes'))

WebUI.click(findTestObject('Collection/span_Compass Slip On Kawung'))

WebUI.click(findTestObject('Collection/span_NH X COMPASS'))

'Based on Color'
WebUI.click(findTestObject('Color/span_Baby Blue'))

WebUI.click(findTestObject('Color/span_Beige'))

WebUI.click(findTestObject('Color/span_Black'))

WebUI.click(findTestObject('Color/span_Black - Black'))

WebUI.click(findTestObject('Color/span_Black - White'))

'Based on Size'
WebUI.click(findTestObject('Size/span_34'))

WebUI.click(findTestObject('Size/div_35'))

WebUI.click(findTestObject('Size/span_36'))

WebUI.click(findTestObject('Size/span_37'))

WebUI.click(findTestObject('Size/span_38'))

